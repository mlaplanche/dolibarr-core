# [2.0.0](https://gitlab.com/mlaplanche/dolibarr-core/compare/v1.2.1...v2.0.0) (2023-09-26)


### Features

* **api:** update restharp to 110.x ([86c8e34](https://gitlab.com/mlaplanche/dolibarr-core/commit/86c8e34683dc70bb9e22b50e9da7a4524d21ac45))


### BREAKING CHANGES

* **api:** Api communication with Authenticator

## [1.2.1](https://gitlab.com/mlaplanche/dolibarr-core/compare/v1.2.0...v1.2.1) (2023-09-21)


### Bug Fixes

* add ProductManager to scoped services ([78daf0b](https://gitlab.com/mlaplanche/dolibarr-core/commit/78daf0b3e4b23fd9a9a391f5da9657e39bd93d47))

# [1.2.0](https://gitlab.com/mlaplanche/dolibarr-core/compare/v1.1.1...v1.2.0) (2023-09-21)


### Features

* add Product endpoint ([a0a8a1c](https://gitlab.com/mlaplanche/dolibarr-core/commit/a0a8a1cb3ad504cfd61593a116bccda988d5db85))

## [1.1.1](https://gitlab.com/mlaplanche/dolibarr-core/compare/v1.1.0...v1.1.1) (2023-07-28)


### Bug Fixes

* update resharp dependency to 110.x ([0323485](https://gitlab.com/mlaplanche/dolibarr-core/commit/0323485b911542d62fd4d4179e97e5b73d573a98))

# [1.1.0](https://gitlab.com/mlaplanche/dolibarr-core/compare/v1.0.3...v1.1.0) (2022-12-19)


### Features

* **core:** add contact and role entity ([8e411e4](https://gitlab.com/mlaplanche/dolibarr-core/commit/8e411e486a6146dd2a8897e2414e35cc548129a4))

## [1.0.3](https://gitlab.com/mlaplanche/dolibarr-core/compare/v1.0.2...v1.0.3) (2022-11-03)


### Bug Fixes

* **core:** catch Unauthorize exception in client ([80c4708](https://gitlab.com/mlaplanche/dolibarr-core/commit/80c47085a1278ec1112678774bb187e9e90a95c4))

## [1.0.2](https://gitlab.com/mlaplanche/dolibarr-core/compare/v1.0.1...v1.0.2) (2022-11-03)


### Bug Fixes

* **log:** change log level for authentication ([d50643c](https://gitlab.com/mlaplanche/dolibarr-core/commit/d50643c26de4cdbb5d543d1bfb1beb747d1f517f))

## [1.0.1](https://gitlab.com/mlaplanche/dolibarr-core/compare/v1.0.0...v1.0.1) (2022-11-03)


### Bug Fixes

* **log:** update log in client ([e99fee0](https://gitlab.com/mlaplanche/dolibarr-core/commit/e99fee0a567e822a1d41ffeae50f7e40690c1dc7))

# 1.0.0 (2022-07-09)


### Bug Fixes

* **common:** fix logger dependency ([e85a7ba](https://gitlab.com/mlaplanche/dolibarr-core/commit/e85a7ba828f60531e403c85da8215cfe6656ad25))
