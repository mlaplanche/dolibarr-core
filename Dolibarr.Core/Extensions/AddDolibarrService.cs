using Dolibarr.Managers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Dolibarr.Extensions;

public static class AddDolibarrService
{
    public static IServiceCollection AddDolibarr(this IServiceCollection services, IConfiguration config)
    {
        var dolibarrConfig = new DolibarrConfig();
        config.GetSection("DolibarrConfig").Bind(dolibarrConfig);

        services.AddSingleton(dolibarrConfig);
        services.AddSingleton<DolibarrClient>();

        // managers
        services.AddScoped<ThirdPartyManager>();
        services.AddScoped<ContactManager>();
        services.AddScoped<ProductManager>();

        return services;
    }
}