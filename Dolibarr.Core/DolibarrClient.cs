using Dolibarr.Helpers;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;

namespace Dolibarr;

/// <inheritdoc />
public class DolibarrClient : RestClient
{
    /// <inheritdoc />
    public DolibarrClient(
        DolibarrConfig dolibarrConfig,
        string customBaseUrl = ""
    ) : base(new RestClientOptions(string.IsNullOrEmpty(customBaseUrl) ? dolibarrConfig.ApiUrl ?? "" : customBaseUrl)
        {
            Authenticator = new DolibarrAuthenticator(
                dolibarrConfig.ApiUrl!,
                dolibarrConfig.Identifier!,
                dolibarrConfig.Password!
            )
        },
        configureSerialization: config => config.UseNewtonsoftJson()
    )
    {
    }

    /// <inheritdoc />
    public DolibarrClient(
        DolibarrConfig dolibarrConfig,
        HttpMessageHandler handler,
        bool disposeHandler = true
    ) : base(
        handler,
        disposeHandler,
        options =>
        {
            options.Authenticator =
                new DolibarrAuthenticator(
                    dolibarrConfig.ApiUrl!,
                    dolibarrConfig.Identifier!,
                    dolibarrConfig.Password!
                );
        },
        config => config.UseNewtonsoftJson()
    )
    {
    }
    //
    // private void Authentication()
    // {
    //     _logger.Log(LogLevel.Information, "--- Dolibarr Authentication ---");
    //     var body = new { login = _config.Identifier, password = _config.Password, reset = 1 };
    //     var request = new RestRequest("login").AddJsonBody(body);
    //     var jsonResponse = _client.Post<dynamic>(request);
    //     var response =
    //         JsonConvert.DeserializeAnonymousType(jsonResponse?.ToString(), new { success = new { token = "" } });
    //
    //     if (_client.DefaultParameters.GetParameters(ParameterType.HttpHeader)
    //             .FirstOrDefault(p => p.Name == "DOLAPIKEY") != null)
    //         _client.DefaultParameters.RemoveParameter("DOLAPIKEY", ParameterType.HttpHeader);
    //     _client.AddDefaultHeader("DOLAPIKEY", $"{response!.success.token}");
    // }
    //
    // public T? Get<T>(RestRequest request)
    // {
    //     _logger.Log(LogLevel.Information, "--- GET REQUEST ---");
    //     _logger.Log(LogLevel.Information, _client.BuildUri(request).ToString());
    //     dynamic? result = null;
    //     try
    //     {
    //         result = _client.Get<dynamic?>(request);
    //         if (result is JContainer arrayResult)
    //         {
    //             _logger.Log(LogLevel.Debug, arrayResult.ToString());
    //             return JsonConvert.DeserializeObject<T>(arrayResult.ToString());
    //         }
    //     }
    //     catch (HttpRequestException e)
    //     {
    //         if (e.StatusCode == HttpStatusCode.Unauthorized)
    //         {
    //             _logger.Log(LogLevel.Warning, "--- Dolibarr Unauthorized ---");
    //             Authentication();
    //             return _client.Get<T?>(request);
    //         }
    //
    //         _logger.Log(LogLevel.Error, e.InnerException, e.Message);
    //         throw;
    //     }
    //     catch (JsonSerializationException e)
    //     {
    //         if (result?.error == null) throw;
    //         if (result.error.code == 404) return default;
    //         throw new HttpRequestException($"Une erreur est survenue: {result.error.messsage}", null,
    //             result.error.code);
    //     }
    //
    //     throw new ApplicationException("Une erreur interne est survenue");
    // }
    //
    // public T? Post<T>(RestRequest request)
    // {
    //     _logger.Log(LogLevel.Information, "--- POST REQUEST ---");
    //     _logger.Log(LogLevel.Information, _client.BuildUri(request).ToString());
    //     foreach (var parameter in request.Parameters)
    //         _logger.Log(LogLevel.Debug, parameter.Name + ": " + JsonConvert.SerializeObject(parameter.Value));
    //     try
    //     {
    //         return _client.Post<T?>(request);
    //     }
    //     catch (HttpRequestException e)
    //     {
    //         if (e.StatusCode == HttpStatusCode.Unauthorized)
    //         {
    //             _logger.Log(LogLevel.Warning, "--- Dolibarr Unauthorized ---");
    //             Authentication();
    //             return _client.Post<T?>(request);
    //         }
    //
    //         _logger.Log(LogLevel.Error, e.InnerException, e.Message);
    //         throw;
    //     }
    // }
    //
    // public T? Put<T>(RestRequest request)
    // {
    //     _logger.Log(LogLevel.Information, "--- PUT REQUEST ---");
    //     _logger.Log(LogLevel.Information, _client.BuildUri(request).ToString());
    //     foreach (var parameter in request.Parameters)
    //         _logger.Log(LogLevel.Debug, parameter.Name + ": " + JsonConvert.SerializeObject(parameter.Value));
    //     try
    //     {
    //         return _client.Put<T?>(request);
    //     }
    //     catch (HttpRequestException e)
    //     {
    //         if (e.StatusCode == HttpStatusCode.Unauthorized)
    //         {
    //             _logger.Log(LogLevel.Warning, "--- Dolibarr Unauthorized ---");
    //             Authentication();
    //             return _client.Put<T?>(request);
    //         }
    //
    //         _logger.Log(LogLevel.Error, e.InnerException, e.Message);
    //         throw;
    //     }
    // }
}