namespace Dolibarr.Interfaces;

public interface IDolibarrEntity
{
    public string? Id { get; set; }
}