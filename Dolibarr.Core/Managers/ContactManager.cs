using Dolibarr.Models;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace Dolibarr.Managers;

public class ContactManager : AbstractGenericManager<Contact>
{
    private readonly DolibarrConfig _dolibarrConfig;
    private readonly ILogger<ContactManager> _logger;

    public ContactManager(DolibarrConfig dolibarrConfig, ILogger<ContactManager> logger)
        :
        base(dolibarrConfig, "contacts", logger)
    {
        _dolibarrConfig = dolibarrConfig;
        _logger = logger;
    }

    public async Task<Contact?> Create(Contact entity)
    {
        var existingContact =
            await GetListAsync(
                new RestRequest().AddQueryParameter("sqlfilters", $"(t.email:like:'{entity.Email}')", false)
            );

        if (existingContact != null && existingContact.Count != 0)
            return existingContact.FirstOrDefault(tp => tp.Email == entity.Email);

        return await PostAsync(entity);
    }
}