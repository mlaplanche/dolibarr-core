using Dolibarr.Models;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace Dolibarr.Managers;

public class ThirdPartyManager : AbstractGenericManager<ThirdParty>
{
    private readonly DolibarrConfig _dolibarrConfig;
    private readonly ILogger<ThirdPartyManager> _logger;

    public ThirdPartyManager(DolibarrConfig dolibarrConfig, ILogger<ThirdPartyManager> logger) :
        base(dolibarrConfig, "thirdparties", logger)
    {
        _dolibarrConfig = dolibarrConfig;
        _logger = logger;
    }

    public async Task<ThirdParty?> Create(ThirdParty entity)
    {
        var existingThirdParty =
            await GetListAsync(
                new RestRequest().AddQueryParameter("sqlfilters", $"(t.email:like:'{entity.Email}')", false));
        if (existingThirdParty != null && existingThirdParty.Count != 0)
            return existingThirdParty.FirstOrDefault(tp => tp.Email == entity.Email);

        return await PostAsync(entity);
    }

    public async Task<ThirdParty?> GetByEmail(string email)
    {
        var request = new RestRequest($"{Endpoint}/email/" + email);
        return await GetAsync(request);
    }

    public string GetDolibarrUrl(string id)
    {
        return _dolibarrConfig.AppUrl + "comm/card.php?socid=" + id;
    }
}