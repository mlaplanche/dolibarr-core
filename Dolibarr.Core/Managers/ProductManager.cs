using Dolibarr.Models;
using Microsoft.Extensions.Logging;

namespace Dolibarr.Managers;

public class ProductManager : AbstractGenericManager<Product>
{
    private readonly DolibarrConfig _dolibarrConfig;
    private readonly ILogger<ProductManager> _logger;

    public ProductManager(DolibarrConfig dolibarrConfig, ILogger<ProductManager> logger)
        : base(dolibarrConfig, "products", logger)
    {
        _dolibarrConfig = dolibarrConfig;
        _logger = logger;
    }
}