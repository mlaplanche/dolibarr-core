using System.Net;

namespace Dolibarr.Models;

public class LoginResponse
{
    public HttpStatusCode Code { get; set; }
    public string Token { get; set; } = string.Empty;
    public string Entity { get; set; } = "0";
    public string Message { get; set; } = "";
}