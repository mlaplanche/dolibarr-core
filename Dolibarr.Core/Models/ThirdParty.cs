using Dolibarr.Interfaces;
using Dolibarr.Utils;
using Newtonsoft.Json;

namespace Dolibarr.Models;

public class ThirdParty : IDolibarrEntity
{
    public string? Name { get; set; }
    public string? Address { get; set; }
    public string? Zip { get; set; }
    public string? Town { get; set; }
    public string? Phone { get; set; }
    public string Client { get; set; } = "3";

    [JsonProperty("code_client")] public string CodeClient { get; set; } = "-1";

    public string? IdProf1 { get; set; }


    [JsonProperty("array_options")]
    [JsonConverter(typeof(EmptyArrayOrDictionaryConverter))]
    public Dictionary<string, string?> Options { get; set; } = new();

    [JsonProperty("note_private")] public string? NotePublic { get; set; }
    public string? Email { get; set; }
    public string? Id { get; set; }
}