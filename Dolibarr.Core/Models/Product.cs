using Dolibarr.Interfaces;
using Newtonsoft.Json;

namespace Dolibarr.Models;

public class Product : IDolibarrEntity
{
    [JsonProperty("entity")] public string? Entity { get; set; }

    [JsonProperty("validateFieldsErrors")] public List<object?> ValidateFieldsErrors { get; set; } = new();

    [JsonProperty("import_key")] public string? ImportKey { get; set; }

    [JsonProperty("array_options")] public List<object?> ArrayOptions { get; set; } = new();

    [JsonProperty("array_languages")] public object? ArrayLanguages { get; set; }

    [JsonProperty("contacts_ids")] public object? ContactsIds { get; set; }

    [JsonProperty("linked_object?s")] public object? LinkedObjects { get; set; }

    [JsonProperty("linkedObjectsIds")] public object? LinkedObjectsIds { get; set; }

    [JsonProperty("linkedObjectsFullLoaded")]
    public List<object?> LinkedObjectsFullLoaded { get; set; } = new();

    [JsonProperty("canvas")] public string? Canvas { get; set; }

    [JsonProperty("fk_projet")] public object? FkProjet { get; set; }

    [JsonProperty("ref")] public string? Ref { get; set; }

    [JsonProperty("ref_ext")] public object? RefExt { get; set; }

    [JsonProperty("status")] public string? Status { get; set; }

    [JsonProperty("country_id")] public object? CountryId { get; set; }

    [JsonProperty("country_code")] public string? CountryCode { get; set; }

    [JsonProperty("state_id")] public object? StateId { get; set; }

    [JsonProperty("region_id")] public object? RegionId { get; set; }

    [JsonProperty("barcode_type")] public object? BarcodeType { get; set; }

    [JsonProperty("barcode_type_coder")] public object? BarcodeTypeCoder { get; set; }

    [JsonProperty("last_main_doc")] public object? LastMainDoc { get; set; }

    [JsonProperty("note_public")] public object? NotePublic { get; set; }

    [JsonProperty("note_private")] public string? NotePrivate { get; set; }

    [JsonProperty("total_ht")] public object? TotalHt { get; set; }

    [JsonProperty("total_tva")] public object? TotalTva { get; set; }

    [JsonProperty("total_localtax1")] public object? TotalLocaltax1 { get; set; }

    [JsonProperty("total_localtax2")] public object? TotalLocaltax2 { get; set; }

    [JsonProperty("total_ttc")] public object? TotalTtc { get; set; }

    [JsonProperty("date_creation")] public string? DateCreation { get; set; }

    [JsonProperty("date_validation")] public object? DateValidation { get; set; }

    [JsonProperty("date_modification")] public string? DateModification { get; set; }

    [JsonProperty("date_cloture")] public object? DateCloture { get; set; }

    [JsonProperty("user_author")] public object? UserAuthor { get; set; }

    [JsonProperty("user_creation")] public object? UserCreation { get; set; }

    [JsonProperty("user_creation_id")] public object? UserCreationId { get; set; }

    [JsonProperty("user_valid")] public object? UserValid { get; set; }

    [JsonProperty("user_validation")] public object? UserValidation { get; set; }

    [JsonProperty("user_validation_id")] public object? UserValidationId { get; set; }

    [JsonProperty("user_closing_id")] public object? UserClosingId { get; set; }

    [JsonProperty("user_modification")] public object? UserModification { get; set; }

    [JsonProperty("user_modification_id")] public object? UserModificationId { get; set; }

    [JsonProperty("specimen")] public int? Specimen { get; set; }

    [JsonProperty("label")] public string? Label { get; set; }

    [JsonProperty("description")] public string? Description { get; set; }

    [JsonProperty("other")] public object? Other { get; set; }

    [JsonProperty("type")] public string? Type { get; set; }

    [JsonProperty("price")] public string? Price { get; set; }

    [JsonProperty("price_ttc")] public string? PriceTtc { get; set; }

    [JsonProperty("price_min")] public string? PriceMin { get; set; }

    [JsonProperty("price_min_ttc")] public string? PriceMinTtc { get; set; }

    [JsonProperty("price_base_type")] public string? PriceBaseType { get; set; }

    [JsonProperty("multiprices")] public List<object?> Multiprices { get; set; } = new();

    [JsonProperty("multiprices_ttc")] public List<object?> MultipricesTtc { get; set; } = new();

    [JsonProperty("multiprices_base_type")]
    public List<object?> MultipricesBaseType { get; set; } = new();

    [JsonProperty("multiprices_min")] public List<object?> MultipricesMin { get; set; } = new();

    [JsonProperty("multiprices_min_ttc")] public List<object?> MultipricesMinTtc { get; set; } = new();

    [JsonProperty("multiprices_tva_tx")] public List<object?> MultipricesTvaTx { get; set; } = new();

    [JsonProperty("prices_by_qty")] public List<object?> PricesByQty { get; set; } = new();

    [JsonProperty("prices_by_qty_list")] public List<object?> PricesByQtyList { get; set; } = new();

    [JsonProperty("multilangs")] public List<object?> Multilangs { get; set; } = new();

    [JsonProperty("default_vat_code")] public object? DefaultVatCode { get; set; }

    [JsonProperty("tva_tx")] public string? TvaTx { get; set; }

    [JsonProperty("localtax1_tx")] public string? Localtax1Tx { get; set; }

    [JsonProperty("localtax2_tx")] public string? Localtax2Tx { get; set; }

    [JsonProperty("localtax1_type")] public string? Localtax1Type { get; set; }

    [JsonProperty("localtax2_type")] public string? Localtax2Type { get; set; }

    [JsonProperty("lifetime")] public object? Lifetime { get; set; }

    [JsonProperty("qc_frequency")] public object? QcFrequency { get; set; }

    [JsonProperty("stock_reel")] public object? StockReel { get; set; }

    [JsonProperty("stock_theorique")] public object? StockTheorique { get; set; }

    [JsonProperty("cost_price")] public object? CostPrice { get; set; }

    [JsonProperty("pmp")] public string? Pmp { get; set; }

    [JsonProperty("seuil_stock_alerte")] public string? SeuilStockAlerte { get; set; }

    [JsonProperty("desiredstock")] public string? Desiredstock { get; set; }

    [JsonProperty("duration_value")] public string? DurationValue { get; set; }

    [JsonProperty("duration_unit")] public string? DurationUnit { get; set; }

    [JsonProperty("status_buy")] public string? StatusBuy { get; set; }

    [JsonProperty("finished")] public object? Finished { get; set; }

    [JsonProperty("fk_default_bom")] public object? FkDefaultBom { get; set; }

    [JsonProperty("status_batch")] public string? StatusBatch { get; set; }

    [JsonProperty("batch_mask")] public string? BatchMask { get; set; }

    [JsonProperty("customcode")] public string? Customcode { get; set; }

    [JsonProperty("url")] public object? Url { get; set; }

    [JsonProperty("weight")] public object? Weight { get; set; }

    [JsonProperty("weight_units")] public object? WeightUnits { get; set; }

    [JsonProperty("length")] public object? Length { get; set; }

    [JsonProperty("length_units")] public object? LengthUnits { get; set; }

    [JsonProperty("width")] public object? Width { get; set; }

    [JsonProperty("width_units")] public object? WidthUnits { get; set; }

    [JsonProperty("height")] public object? Height { get; set; }

    [JsonProperty("height_units")] public object? HeightUnits { get; set; }

    [JsonProperty("surface")] public object? Surface { get; set; }

    [JsonProperty("surface_units")] public object? SurfaceUnits { get; set; }

    [JsonProperty("volume")] public object? Volume { get; set; }

    [JsonProperty("volume_units")] public object? VolumeUnits { get; set; }

    [JsonProperty("net_measure")] public object? NetMeasure { get; set; }

    [JsonProperty("net_measure_units")] public object? NetMeasureUnits { get; set; }

    [JsonProperty("accountancy_code_sell")]
    public string? AccountancyCodeSell { get; set; }

    [JsonProperty("accountancy_code_sell_intra")]
    public string? AccountancyCodeSellIntra { get; set; }

    [JsonProperty("accountancy_code_sell_export")]
    public string? AccountancyCodeSellExport { get; set; }

    [JsonProperty("accountancy_code_buy")] public string? AccountancyCodeBuy { get; set; }

    [JsonProperty("accountancy_code_buy_intra")]
    public string? AccountancyCodeBuyIntra { get; set; }

    [JsonProperty("accountancy_code_buy_export")]
    public string? AccountancyCodeBuyExport { get; set; }

    [JsonProperty("barcode")] public object? Barcode { get; set; }

    [JsonProperty("stock_warehouse")] public List<object?> StockWarehouse { get; set; } = new();

    [JsonProperty("fk_default_warehouse")] public object? FkDefaultWarehouse { get; set; }

    [JsonProperty("fk_price_expression")] public object? FkPriceExpression { get; set; }

    [JsonProperty("fk_unit")] public object? FkUnit { get; set; }

    [JsonProperty("price_autogen")] public string? PriceAutogen { get; set; }

    [JsonProperty("is_object?_used")] public object? IsObjectUsed { get; set; }

    [JsonProperty("mandatory_period")] public string? MandatoryPeriod { get; set; }

    [JsonProperty("duration")] public string? Duration { get; set; }
    [JsonProperty("id")] public string? Id { get; set; }
}