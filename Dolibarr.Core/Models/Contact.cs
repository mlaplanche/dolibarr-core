using Dolibarr.Interfaces;
using Newtonsoft.Json;

namespace Dolibarr.Models;

public class Contact : IDolibarrEntity
{
    [JsonProperty("statut")] public string? Statut { get; set; }
    [JsonProperty("lastname")] public string? Lastname { get; set; }
    [JsonProperty("firstname")] public string? Firstname { get; set; }
    [JsonProperty("roles")] public dynamic? Roles { get; set; }
    [JsonProperty("socid")] public string? SocId { get; set; }
    [JsonProperty("fk_soc")] public string? FkSoc { get; set; }
    [JsonProperty("email")] public string? Email { get; set; }
    public string? Id { get; set; }
}