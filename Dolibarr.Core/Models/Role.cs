using Newtonsoft.Json;

namespace Dolibarr.Models;

public class Role
{
    [JsonProperty("id")] public string? Id { get; set; } = "";

    [JsonProperty("socid")] public string? SocId { get; set; }
    public string? Element { get; set; }
    public string? Source { get; set; }
    public string? Code { get; set; }
    public string? Label { get; set; }
}