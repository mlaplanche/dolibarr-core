namespace Dolibarr.Models;

public class WrapperLoginResponse
{
    public LoginResponse? Success { get; set; }
}