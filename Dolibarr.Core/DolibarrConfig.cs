namespace Dolibarr;

public class DolibarrConfig
{
    public string? ApiUrl { get; set; }
    public string? AppUrl { get; set; }
    public string? Identifier { get; set; }
    public string? Password { get; set; }
}