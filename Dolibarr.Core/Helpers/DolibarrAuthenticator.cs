using Dolibarr.Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers.NewtonsoftJson;

namespace Dolibarr.Helpers;

public class DolibarrAuthenticator : AuthenticatorBase
{
    private readonly string _baseUrl;
    private readonly string _password;
    private readonly string _username;


    public DolibarrAuthenticator(string baseUrl, string username, string password) : base("")
    {
        _baseUrl = baseUrl;
        _username = username;
        _password = password;
    }

    protected override async ValueTask<Parameter> GetAuthenticationParameter(string accessToken)
    {
        Token = string.IsNullOrEmpty(Token) ? await GetToken() : Token;

        return new HeaderParameter("DOLAPIKEY", Token);
    }

    private async Task<string> GetToken()
    {
        var options = new RestClientOptions(_baseUrl);

        using var client = new RestClient(
            options,
            configureSerialization: config => config.UseNewtonsoftJson()
        );

        var body = new { login = _username, password = _password, reset = 0 };
        var request = new RestRequest("login").AddJsonBody(body);
        var response = await client.ExecutePostAsync(request);

        if (response.IsSuccessful)
        {
            var tokenObject = JsonConvert.DeserializeObject<WrapperLoginResponse>(response.Content!);

            return tokenObject?.Success?.Token ?? "";
        }

        response.ThrowIfError();

        return string.Empty;
    }
}